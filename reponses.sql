USE asterixdb

sudo mysql -Dasterixdb -unicolas -psimplon</home/nicolas/Documents/Exercices/SQL/gaulois/asterix_db.sql
 
 -- Question 1 - Liste des potions : Numéro, libellé, formule et constituant principal. (5 lignes)
 SELECT * FROM potion;

 -- Question 2 - Liste des noms des trophées rapportant 3 points. (2 lignes)
SELECT NomCateg, NbPoints 
FROM categorie
WHERE NbPoints=3;

-- Question 3 - Liste des villages (noms) contenant plus de 35 huttes. (4 lignes)
SELECT NomVillage 
FROM village
WHERE NbHuttes>35;

-- Question 4 - Liste des trophées (numéros) pris en mai / juin 52. (4 lignes)
SELECT NumTrophee FROM trophee
WHERE YEAR(DatePrise) = 2052 AND (MONTH(DatePrise) = 5 OR MONTH(DatePrise) = 6);

-- Question 5 - Noms des habitants commençant par 'a' et contenant la lettre 'r'. (3 lignes)
SELECT Nom FROM habitant
WHERE Nom LIKE 'a%r%';

-- Question 6 - Numéros des habitants ayant bu les potions numéros 1, 3 ou 4. (8 lignes)
SELECT DISTINCT h.NumHab FROM habitant AS h
JOIN absorber AS a ON h.NumHab = a.NumHab
WHERE NumPotion = "1" OR NumPotion = "3" OR NumPotion = "4";

-- Question 7 - Liste des trophées : numéro, date de prise, nom de la catégorie et nom du preneur. (10 lignes)
SELECT t.NumTrophee, t.DatePrise, c.NomCateg, h.Nom
FROM trophee AS t
JOIN categorie AS c ON t.CodeCat = c.CodeCat
JOIN habitant AS h ON t.NumPreneur = h.NumHab;

-- Question 8 - Nom des habitants qui habitent à Aquilona. (7 lignes)
SELECT Nom from habitant AS h
JOIN village AS v ON h.NumVillage = v.NumVillage
WHERE NomVillage = "Aquilona";

-- Question 9 - Nom des habitants ayant pris des trophées de catégorie Bouclier de Légat. (2 lignes)
SELECT h.Nom FROM habitant AS h
JOIN trophee AS t ON h.NumHab = t.NumPreneur
JOIN categorie AS c ON t.CodeCat = c.CodeCat
WHERE NomCateg = "Bouclier de Légat";

 -- Question 10 - Liste des potions (libellés) fabriquées par Panoramix : libellé, formule et constituant principal. (3 lignes)
SELECT p.LibPotion, p.Formule, p.ConstituantPrincipal
FROM potion AS p 
JOIN fabriquer AS f ON p.NumPotion = f.NumPotion
JOIN habitant AS h ON f.NumHab = h.NumHab
WHERE h.Nom = "Panoramix";

-- Question 11 - Liste des potions (libellés) absorbées par Homéopatix. (2 lignes)
SELECT DISTINCT p.LibPotion FROM potion AS p
JOIN absorber AS a ON p.NumPotion = a.NumPotion
JOIN habitant AS h ON a.NumHab = h.NumHab
WHERE h.Nom = "Homéopatix";

-- Question 12 - Liste des habitants (noms) ayant absorbé une potion fabriquée par l'habitant numéro 3. (4 lignes)
SELECT DISTINCT h.Nom FROM habitant AS h
JOIN absorber AS a ON h.NumHab = a.NumHab
JOIN fabriquer AS f ON a.NumPotion = f.NumPotion
WHERE f.NumHab = 3;

-- Question 13 - Liste des habitants (noms) ayant absorbé une potion fabriquée par Amnésix. (7 lignes)
SELECT * FROM habitant;
SELECT DISTINCT h.Nom FROM habitant AS h
JOIN absorber AS a ON h.NumHab = a.NumHab
JOIN fabriquer AS f ON a.NumPotion = f.NumPotion
JOIN habitant AS hab ON f.NumHab = hab.NumHab
WHERE hab.nom = "Amnésix";

-- Question 14 - Nom des habitants dont la qualité n'est pas renseignée. (3 lignes)
SELECT Nom FROM habitant 
WHERE NumQualite IS NULL;

-- Question 15 - Nom des habitants ayant consommé la potion magique n°1 (c'est le libellé de la potion) en février 52. (3 lignes)

    SELECT h.nom FROM habitant h
    -> JOIN absorber a ON h.NumHab = a.NumHab
    -> JOIN potion p ON a.NumPotion = p.NumPotion
    -> WHERE p.LibPotion = 'Potion magique n°1' AND MONTH(a.DateA)

-- Question 16 - Nom et âge des habitants par ordre alphabétique. (22 lignes)
SELECT Nom, Age
    -> FROM habitant
    -> ORDER BY Nom ASC;

-- Question 17 - Liste des resserres classées de la plus grande à la plus petite : nom de resserre et nom du village. (3 lignes)
SELECT NomResserre, NumVillage
    -> FROM resserre
    -> ORDER BY Superficie DESC;

-- Question 18 - Nombre d'habitants du village numéro 5. (4)
SELECT COUNT(*) FROM habitant
    -> WHERE NumVillage=5;

-- Question 19 - Nombre de points gagnés par Goudurix. (5)
SELECT count(*) FROM habitant Hab
    JOIN trophee t ON Hab.NumHab = t.NumPreneur
     JOIN categorie c ON t.CodeCat = c.CodeCat
    WHERE Nom = "Goudurix";

 -- Question 20 - Date de la première prise de trophée
SELECT MIN(DatePrise) FROM trophee
ORDER BY DatePrise;

 -- Question 21 : Nombre de louches de potion magique n°2 (c'est le libellé de la potion) absorbées. (19)
SELECT * FROM potion;
SELECT * FROM absorber;
SELECT SUM(quantite) FROM potion AS p
JOIN absorber AS a ON p.NumPotion = a.NumPotion
WHERE LibPotion = "Potion magique n°2";

 -- Question 22 -Superficie la plus grande. (895)
SELECT Max(Superficie) FROM resserre;

 -- Question 23 - Nombre d'habitants par village (nom du village, nombre). (7 lignes)
 SELECT * FROM village
 SELECT NomVillage, count(NumHab) FROM habitant AS h
 JOIN village AS v ON h.NumVillage = v.NumVillage
 GROUP BY NomVillage;

 -- Question 24 - Nombre de trophées par habitant (6 lignes)
SELECT * FROM habitant;
SELECT * FROM trophee;
SELECT NumPreneur, count(*) FROM trophee AS t
JOIN habitant AS h ON t.NumPreneur = h.NumHab
GROUP BY t.NumPreneur;

-- Question 25 - Moyenne d'âge des habitants par province (nom de province, calcul). (3 lignes)

SELECT p.NomProvince,AVG(h.Age) AS MoyenneAge
FROM habitant AS h
JOIN village AS v ON h.NumVillage = v.NumVillage
JOIN province AS p ON v.NumProvince = p.NumProvince
GROUP BY p.NomProvince;

-- Question 26 - Nombre de potions différentes absorbées par chaque habitant (nom et nombre). (9 lignes)
SELECT Nom, count(DISTINCT LibPotion) AS "Nbre de potions différentes absorbées" 
FROM habitant AS h
JOIN absorber AS a ON h.NumHab = a.NumHab
JOIN potion AS p ON a.NumPotion = p.NumPotion
GROUP BY Nom;

-- Question 27 - Nom des habitants ayant bu plus de 2 louches de potion zen. (1 ligne)
SELECT * FROM potion;
SELECT Nom, a.Quantite AS "Louches de potion Zen" 
FROM habitant as h
JOIN absorber AS a ON h.NumHab = a.NumHab
JOIN potion AS p ON a.NumPotion = p.NumPotion
WHERE a.Quantite > 2 AND LibPotion = "Potion Zen";

 -- Question 28 - Noms des villages dans lesquels on trouve une resserre (3 lignes)
 
 SELECT NomVillage from village AS v
 JOIN resserre AS r ON v.NumVillage = r.NumVillage;

 -- Question 29 - Nom du village contenant le plus grand nombre de huttes. (Gergovie)
SELECT * FROM village;
SELECT NomVillage, MAX(NbHuttes) AS "le plus grand nombre de huttes" from village
GROUP BY NomVillage
LIMIT 1;

CREATE OR REPL

-- Question 30 - Noms des habitants ayant pris plus de trophées qu'Obélix (3 lignes).
SELECT * FROM habitant;
SELECT * FROM trophee;
SELECT h.Nom AS "habitant_ayant_des_trophées", count(NumPreneur) AS "Nbre de trophées"  
FROM habitant AS h 
JOIN trophee AS t ON h.NumHab = t.NumPreneur
GROUP BY Nom;



